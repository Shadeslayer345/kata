/** Various helper functions, used in tests */

const
/**
 * Composable map function. Allows a cleaner syntax when transforming the thing
 *     to map
 * @param  {Array} x What we want to mutate
 * @param  {func} f How we want to mutate
 * @return {Array} f(x) or the updated array with mutated elements
 */
  map = (x, f) => {
    return x.map(f);
  },

/**
 * Composable reduce function. Allowsa cleaner syntax when transforming the
 *     thing to reduce
 * @param  {Array} x What we want to reduce
 * @param  {func} f How we want to reduce
 * @return {Number|String} f(x) or the value reduced from the array
 */
  reduce = (x, f) => {
    return x.reduce(f);
  },

/**
 * [description]
 * @param  {String|Number} x First value
 * @param  {String|Number} y Second Value
 * @return {Number} x+y or values added together
 */
  sum = (x, y) => {
    return parseInt(x, 10) + parseInt(y, 10);
  };

export default {
  map,
  reduce,
  sum
};
