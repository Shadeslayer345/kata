/* Using Luhn algorithm to validate credit card numnbers */

const
  /**
   * Doubles even positioned numbers
   * @param  {number} x Number to mutate
   * @param  {number} index index of number to mutate
   * @return {number} index is even => x * 2
   *                  index is odd => x
   */
  doubleEven = (x, index) => {
    if ((index + 1) % 2 === 0) {
      return x * 2;
    }
    return x;
  },

  /**
   * Lowers a number by 9 if it is greater
   * @param  {number} x number to mutate
   * @return {number} x > 9 => x - 9
   *                  x <= 9 => x
   */
  lowerGreaterThanNine = (x) => {
    return x > 9 ? parseInt(x, 10) - 9 : parseInt(x, 10);
  },

  /**
   * Adds two values
   * @param  {number} x first number
   * @param  {number} y second number
   * @return {number} x + y
   */
  sum = (x, y) => {
    return parseInt(x, 10) + parseInt(y, 10);
  },

  /**
   * First step of Luhn algorithm, double ever other number starting from the
   *     last index of the list
   * @param  {Array.string} x List of numbers to mutate
   * @return {Array.string} mutated list
   */
  stageOne = (x) => {
    return x.reverse().map(doubleEven).reverse();
  },

  /**
   * Second step of Luhn algorithm, lower any number greater than 9 by 9
   * @param  {Array.string} x List of numbers to mutate
   * @return {Arrat.string} mutated list
   */
  stageTwo = (x) => {
    return x.map(lowerGreaterThanNine);
  },

  /**
   * Third step of Luhn algorithm, sum all numbers
   * @param  {Array.string} x List of numbers to sum
   * @return {number} sum of all elements in list
   */
  stageThree = (x) => {
    return x.reduce(sum);
  },

  /**
   * Last step of Luhn algorithm, determine if number is divisible by 10
   * @param  {number} x Number to validate
   * @return {bool} true => x divisible by 10
   *                false => x not divisible by 10
   */
  stageFour = (x) => {
    return x % 10 === 0;
  };

/**
 * Complete Luhn algorithm implementation, validates a number based on a series
 *     of tests. (See previous functions for details)
 * @param  {number} x Max 16 digit number
 * @return {bool} true => x is valid
 *                false => x is not valid
 */
export default (x) => {
  const xArr = x.toString().split(``);

  return stageFour(stageThree(stageTwo(stageOne(xArr))));
};

export {
  doubleEven,
  lowerGreaterThanNine,
  sum,
  stageOne,
  stageTwo,
  stageThree,
  stageFour
};
