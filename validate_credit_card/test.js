/* eslint no-magic-numbers: 0 */
import validate, {
  doubleEven,
  lowerGreaterThanNine,
  sum,
  stageOne,
  stageTwo,
  stageThree,
  stageFour
} from './main';
import addAssertions from 'extend-tape';
import tape from 'tape';
import arrEq from 'tape-arr-equals';

const test = addAssertions(tape, {arrEq});

test(`validate credit card numbers module`, (swear) => {
  swear.plan(2);
  const
    testStageCardOne = [1, 2, 3, 4, 5],
    testStageCardTwo = [1, 2, 3, 4],
    testStageCardThree = [2, 6, 9, 10, 6, 14, 8, 18, 1, 0, 4, 3],

    validateCardOne = 12345,
    validateCardTwo = 12344;

  test(`validate stageOne - double special numbers`, (swear) => {
    swear.plan(2);
    test(`validate doubleEven - double every other element`, (swear) => {
      swear.plan(2);
      swear.equal(doubleEven(0, 0), 0, `odd & 0 validated`);
      swear.equal(doubleEven(1, 1), 2, `evens validated`);
    });
    swear.arrEq(stageOne(testStageCardOne), [1, 4, 3, 8, 5],
          `odd length list validated`);
    swear.arrEq(stageOne(testStageCardTwo), [2, 2, 6, 4],
          `even length list validated`);
  });

  test(`validate stageTwo - lower elements greater than 9 by 9`, (swear) => {
    swear.plan(3);
    test(`validate lowerGreaterThanNine - lower number by 9`, (swear) => {
      swear.plan(3);
      swear.equal(lowerGreaterThanNine(0), 0, `lower than 9 validated`);
      swear.equal(lowerGreaterThanNine(9), 9, `9 validated`);
      swear.equal(lowerGreaterThanNine(10), 1, `greater than 9 validated`);
    });
    swear.arrEq(stageTwo(testStageCardOne), [1, 2, 3, 4, 5],
          `All elements lower than 9 validated`);
    swear.arrEq(stageTwo([10, 11, 12, 18]), [1, 2, 3, 9],
          `All elements greater than 9 validated`);
    swear.arrEq(stageTwo(testStageCardThree,
          [2, 6, 9, 1, 6, 5, 8, 9, 1, 0, 4, 3]),
          `Mixed elements validated`);
  });

  test(`validate stageThree - sum elements`, (swear) => {
    swear.plan(1);
    test(`validate sum - add two ints`, (swear) => {
      swear.plan(1);
      swear.equal(sum(1, 0), 1, `basic addition validated`);
    });
    swear.equal(stageThree(testStageCardThree), 81, `array summing validated`);
  });

  test(`validate stageFour - number divisible by 10`, (swear) => {
    swear.plan(3);
    const
      testStageCardOneSum = 15,
      testStageCardTwoSum = 10;

    swear.notOk(stageFour(testStageCardOneSum), `10 validated`);
    swear.ok(stageFour(testStageCardTwoSum),
          `greater than 10 but divisible validated`);
    swear.notOk(stageFour(8), `less than 10 validated`);
  });

  swear.notOk(validate(validateCardOne), `incorrect validated`);
  swear.ok(validate(validateCardTwo), ` correct validated`);
});
